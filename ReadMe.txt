#skicco-angular
Front end for the SKICCO.
All Rights Reserved in the name of SKICCO Ltd.

#Technologies, license and third party frame works used.
This application is licensed from MIT. All the softwares used here are subjected to licenses and terms of respective owners
angular
Typescript
W3 css
Angular material
angular2-notifications
bootstrap

#Archeticture
skicco-angular
|
 app.module.ts
 app.component.ts
 app-routing.module.ts -- Routing based on lazyloading.
|
 admin --Admin module deals with all the components required for the admin view of the application.
 core  --Core consists of all the components that uses in the application core level Eg., services,model objects,universal loading components.
 errors -- Errors module will be dealing with all the error routes Eg.,404-page not found, 500 etc.,
 feature -- Feature module deals with all the basic components that requires for routing in the application level.
 forms -- This module will be ALL form components which will collect data required for the aplication functionality.
 generic -- This module will be having all the components that are common in most of the views Eg., header,footer etc.,
 skicco-support -- This module is for the support staff Eg., Telecallers.
|
 assets-- all the assets other than coding will go into this directory Eg., styles, images.

    

