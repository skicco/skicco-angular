import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NotificationsService } from "angular2-notifications";
import { Ng2NotificationConstants } from "./core/_constants/ng2-notifications";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private notificationsService: NotificationsService
  ) {}

  ngOnInit() {}
}
