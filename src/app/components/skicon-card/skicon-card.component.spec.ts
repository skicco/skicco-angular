import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiconCardComponent } from './skicon-card.component';

describe('SkiconCardComponent', () => {
  let component: SkiconCardComponent;
  let fixture: ComponentFixture<SkiconCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkiconCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiconCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
