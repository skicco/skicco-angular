import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiccoProjectComponent } from './skicco-project.component';

describe('SkiccoProjectComponent', () => {
  let component: SkiccoProjectComponent;
  let fixture: ComponentFixture<SkiccoProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkiccoProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiccoProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
