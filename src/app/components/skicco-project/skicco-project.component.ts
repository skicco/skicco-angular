import { Component, OnInit, Input } from "@angular/core";
import { ProjectResponse } from "../../core/model/responses/project-response";
import { Router } from "@angular/router";
import { DataSharedService } from "../../core/service/data.service";

@Component({
  selector: "skicco-project",
  templateUrl: "./skicco-project.component.html",
  styleUrls: ["./skicco-project.component.css"]
})
export class SkiccoProjectComponent implements OnInit {
  @Input() project: ProjectResponse;
  @Input() userSkills: any[];
  /*  = [
    
    "XHTML",
    "CSS",
    "HTML",
    "IBM WebSphere",
    "Spring Framework",
    "Struts and Tiles"
  ]; */
  matchingProjects: ProjectResponse;
  isFavourite: boolean;
  skills: any;
  isShownInterest: boolean;
  skillMatch: number;
  constructor(
    private router: Router,
    private _dataService: DataSharedService
  ) {}

  ngOnInit() {
    this.matchingProjects = this.project;
    this.skills = this.project.subSkills;
    this.matchSkills();
  }

  matchSkills() {
    this.skills.sort();
    this.userSkills.sort();
    let tempArray: any[] = [];
    this.skills.forEach(skill => {
      if (this.userSkills.indexOf(skill) > -1) {
        tempArray.push(skill);
      }
    });
    let match = (tempArray.length / this.skills.length) * 100;
    this.skillMatch = +match.toFixed(2);
  }
  showInterest() {
    /* alert("interest shown"); */
  }
  projectTitleClick(project: any) {
    // alert(project.title+" navigated");
    this._dataService.setProjectReponseData(project);
    this.router.navigate(["/project-details"]);
  }
}
