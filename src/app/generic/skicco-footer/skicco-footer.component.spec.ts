import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiccoFooterComponent } from './skicco-footer.component';

describe('SkiccoFooterComponent', () => {
  let component: SkiccoFooterComponent;
  let fixture: ComponentFixture<SkiccoFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkiccoFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiccoFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
