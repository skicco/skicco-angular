import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiccoHeaderComponent } from './skicco-header.component';

describe('SkiccoHeaderComponent', () => {
  let component: SkiccoHeaderComponent;
  let fixture: ComponentFixture<SkiccoHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkiccoHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiccoHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
