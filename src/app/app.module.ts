import { StateModule } from './state/state.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { AmazingTimePickerModule } from 'amazing-time-picker';

import { AppComponent } from './app.component';
import { SkiccoHeaderComponent } from './generic/skicco-header/skicco-header.component';
import { SkiccoFooterComponent } from './generic/skicco-footer/skicco-footer.component';
import { SkiccoLoadingComponent } from './core/skicco-loading/skicco-loading.component';
import { ErrorsModule } from './errors/errors.module';

import { DataSharedService } from './core/service/data.service';
import { DataLoadingComponent } from './components/data-loading/data-loading.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    SkiccoHeaderComponent,
    SkiccoFooterComponent,
    SkiccoLoadingComponent,
    DataLoadingComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    CoreModule,
    ErrorsModule,
    AppRoutingModule,
    HttpClientModule,
    AmazingTimePickerModule,
    SimpleNotificationsModule.forRoot(),
    StateModule,
  ],
  providers: [DataSharedService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent],
})
export class AppModule {}
