import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { HirerService } from "./../../core/service/hirer.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { phoneNumberValidator } from "../../core/validators/phone-number.validator";
import { HirerDetails } from "src/app/core/model/hirer-details";
import { SkiccoLoadingService } from "src/app/core/service/skicco-loading.service";
import { NotificationsService } from "angular2-notifications";
import { resultMemoize } from "@ngrx/store";
@Component({
  selector: "skicco-hirer-details",
  templateUrl: "./hirer-details.component.html",
  styleUrls: ["./hirer-details.component.css"]
})
export class HirerDetailsComponent implements OnInit {
  clientType: string = "individual";
  hirerDetailsForm: FormGroup;
  PHONE_NUMBER = "/^(?([0-9]{3}))?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/";
  hirerDetails: HirerDetails;
  huid: string;
  private notification: any = {
    timeOut: 5000,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true,
    animate: "fromRight"
  };
  constructor(
    private hirerService: HirerService,
    private router: Router,
    public snackBar: MatSnackBar,
    private skiccoLoadingService: SkiccoLoadingService,
    private notificationsService: NotificationsService
  ) {
    this.hirerDetailsForm = this.createFormGroup();
  }

  ngOnInit() {
    this.huid = localStorage.getItem("hirerDetailsToken");
  }

  createFormGroup() {
    return new FormGroup(
      {
        firstName: new FormControl("", [Validators.required]),
        lastName: new FormControl("", [Validators.required]),
        companyName: new FormControl(""),
        position: new FormControl(""),
        addr: new FormControl("", [Validators.required]),
        city: new FormControl("", [Validators.required]),
        state: new FormControl("", [Validators.required]),
        country: new FormControl("", [Validators.required]),
        zipCode: new FormControl("", [
          Validators.required,
          phoneNumberValidator,
          Validators.minLength(6)
        ]),
        mobileNumber: new FormControl("", [
          Validators.required,
          phoneNumberValidator,
          Validators.minLength(10)
        ])
      },
      { updateOn: "blur" }
    );
  }

  /*  get f() {
    return this.hirerDetailsForm.controls;
  } */

  submit() {
    this.skiccoLoadingService.show();
    if (this.hirerDetailsForm.valid) {
      this.hirerDetails = {
        huid: this.huid,
        firstName: this.hirerDetailsForm.value.firstName,
        lastName: this.hirerDetailsForm.value.lastName,
        clientType: this.clientType,
        companyName: this.hirerDetailsForm.value.companyName,
        position: this.hirerDetailsForm.value.position,
        mobileNumber: this.hirerDetailsForm.value.mobileNumber,
        isHirerVerified: false,
        hirerAddressDTO: {
          addr: this.hirerDetailsForm.value.addr,
          city: this.hirerDetailsForm.value.city,
          state: this.hirerDetailsForm.value.state,
          country: this.hirerDetailsForm.value.country,
          zipCode: this.hirerDetailsForm.value.zipCode
        }
      };
      /* this.hirerDetails.firstName = this.hirerDetailsForm.value.firstName;
      this.hirerDetails.lastName = this.hirerDetailsForm.value.lastName;
      this.hirerDetails.clientType = this.hirerDetailsForm.value.clientType;
      this.hirerDetails.position = this.hirerDetailsForm.value.position;
      this.hirerDetails.companyName = this.hirerDetailsForm.value.companyName;
      this.hirerDetails.huid = this.huid;
      this.hirerDetails.hirerAddressDTO.addr = this.hirerDetailsForm.value.addr;
      this.hirerDetails.hirerAddressDTO.city = this.hirerDetailsForm.value.city;
      this.hirerDetails.hirerAddressDTO.state = this.hirerDetailsForm.value.state;
      this.hirerDetails.hirerAddressDTO.zipCode = this.hirerDetailsForm.value.zipCode;
      this.hirerDetails.hirerAddressDTO.country = this.hirerDetailsForm.value.country; */
      this.hirerService.saveHirerDetails(this.hirerDetails).subscribe(
        resp => {
          if (resp.body== "200") {
            this.skiccoLoadingService.hide();
            this.router.navigate(["/hirer/dashboard"]);
          } else {
            this.skiccoLoadingService.hide();
            let msg = "Oops.!! Something went wrong please comeback again..";
            this.notificationsService.error(
              "Internal Server error.",
              msg,
              this.notification
            );
          }
        },
        error => {
          this.skiccoLoadingService.hide();
          let msg = "Oops.!! Something went wrong please comeback again..";
          this.notificationsService.error(
            "Internal Server error.",
            msg,
            this.notification
          );
        }
      );
    } else {
      this.skiccoLoadingService.hide();
      let msg = "There are errors in the form please check.";
      this.snackBar.open(msg, "close", {
        duration: 5000
      });
    }
  }

  setClientType(clientType: any) {
    const companyNameControl = this.hirerDetailsForm.get("companyName");
    const positionControl = this.hirerDetailsForm.get("position");
    this.clientType = clientType;
    if (clientType == "corporate") {
      companyNameControl.setValidators([Validators.required]);
      positionControl.setValidators([Validators.required]);
    } else {
      companyNameControl.clearValidators();
      positionControl.clearValidators();
    }
  }

  //call this in ngOnInIt()
  /* setUserCategoryValidators() {
    const companyName = this.hirerDetailsForm.get('companyName');
    const companyControl = this.hirerDetailsForm.get('company');
    const salaryControl = this.hirerDetailsForm.get('salary');

    this.form.get('userCategory').valueChanges
      .subscribe(userCategory => {

        if (userCategory === 'student') {
          institutionControl.setValidators([Validators.required]);
          companyControl.setValidators(null);
          salaryControl.setValidators(null);
        }

        if (userCategory === 'employee') {
          institutionControl.setValidators(null);
          companyControl.setValidators([Validators.required]);
          salaryControl.setValidators([Validators.required]);
        }

        institutionControl.updateValueAndValidity();
        companyControl.updateValueAndValidity();
        salaryControl.updateValueAndValidity();
      });
  } */
}
