import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HirerDetailsComponent } from './hirer-details.component';

describe('HirerDetailsComponent', () => {
  let component: HirerDetailsComponent;
  let fixture: ComponentFixture<HirerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HirerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HirerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
