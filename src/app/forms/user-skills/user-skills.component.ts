import { Component, OnInit } from '@angular/core';
import { Renderer2 } from '@angular/core';
import { SkillsService } from '../../core/service/skills.service';
import { Router } from '@angular/router';
import { SkiccoLoadingService } from '../../core/service/skicco-loading.service';
declare var $: any;

@Component({
  selector: 'app-user-skills',
  templateUrl: './user-skills.component.html',
  styleUrls: ['./user-skills.component.css'],
})
export class UserSkillsComponent implements OnInit {
  skillDetails: any;
  showSkillDetail = 9;
  category: string;
  subSkillSelect: string;
  ratingClicked: number;
  itemIdRatingClicked: number;
  userSkillDetails: any[] = [
    {
      id: -1,
      rating: 3,
      subSkill: 'English Speaking',
      skill: 'speaking',
    },
  ];
  //private updatedAuthToken: string = localStorage.getItem('userDetailsToken') + '205';
  public userSkillRequestObj: RequestObj[];
  skillList: any[] = [0];
  requestObj: RequestObj;
  skillBtnText: string = 'submit';
  loading: boolean = false;
  noRating: number = 0;
  constructor(
    private render: Renderer2,
    private skillsService: SkillsService,
    private router: Router,
    private skiccoLoadingService: SkiccoLoadingService
  ) {}
  ngOnInit() {
    this.category = 'webDesign';
    this.getSkills();
  }

  getSkills() {
    this.skillsService.getSkillsDetails().subscribe((result) => {
      this.skillDetails = result;
    });
  }

  /**
   * @param skillGroup
   * @param category
   * This method will switch between views on clicking upon particular category by taking
   * the above params as input.
   */
  showSkills(skillGroup, category) {
    this.showSkillDetail = skillGroup;
    this.category = category;
  }

  /**
   * This method is accepting the following as inputs from HTML
   * @param event
   * @param subSkill
   * @param displayKey
   * @param index
   * @param skill
   * This will change the selected class to 'checked' and add toselected table if the
   * skill is not added in the list.
   */
  addSubSkill(event: any, subSkill, displayKey, index, skill) {
    let id = displayKey.toString() + index;
    if (!this.skillList.some((x) => x === id)) {
      event.preventDefault();
      this.skillList.push(id);
      this.render.addClass(event.target, 'checked');
      this.userSkillDetails.push({
        subSkill: subSkill,
        rating: '',
        id: id,
        skill: skill,
      });
    } else {
      var x = document.getElementById('snackbar');
      x.className = 'show';
      setTimeout(function () {
        x.className = x.className.replace('show', '');
      }, 3000);
    }
  }
  /**
   * @param index
   * @param id
   * This method is for removing the selected skill from the skill table.
   */
  deleteRow(index, id) {
    $('#' + id).removeClass('checked');
    this.userSkillDetails.splice(index, 1);
    this.skillList.splice(index, 1);
  }

  /****************************************************************************
   * Consuming saveUserSkillDetails rest API on clicking upon the save button *
   ****************************************************************************/

  saveSkillDetails() {
    this.skiccoLoadingService.show();
    this.noRating = 0;
    this.skillBtnText = 'Submitting...';
    this.requestObj = {
      userSkills: [],
    };

    this.userSkillDetails.forEach((userSkillDetail) => {
      if (userSkillDetail.rating == null || userSkillDetail.rating == '' || userSkillDetail.rating == undefined) {
        this.noRating++;
      }
    });
    if (this.noRating === 0) {
      for (var i = 0; i < this.userSkillDetails.length; i++) {
        delete this.userSkillDetails[i]['id'];
      }
      this.requestObj.userSkills = this.userSkillDetails;
      this.skillsService.saveUSerSkillDetails(this.requestObj).subscribe(
        (result) => {
          if (result.body == '200') {
            let authorization = result.headers.get('Authorization');
            localStorage.setItem('status-token', authorization + 205);
            this.skiccoLoadingService.hide();
            this.router.navigate(['/skicon/dashboard']);
          }
        },
        (error) => {
          this.skiccoLoadingService.hide();
          this.skillBtnText = 'submit';
          ('Oops.!! Something went wrong please comeback again..');
          alert('Oops.!! Something went wrong please comeback again..');
          /* this.alert = "Oops.!! Something went wrong please comeback again..";
          this.snackBar = true; */
        }
      );
      //console.log(this.postResponse.responseMsg);
    } else {
      this.skiccoLoadingService.hide();
      alert('please provide rating for ' + this.noRating + ' skills');
    }
  }
  /**
   *
   * @param clickObj
   */
  ratingComponetClick(clickObj: any): void {
    var userSkillDetail = this.userSkillDetails.filter((item: any) => item.id === clickObj.itemId);
    if (!!userSkillDetail && userSkillDetail.length === 1) {
      userSkillDetail[0].rating = clickObj.rating;
      this.ratingClicked = clickObj.rating;
      this.itemIdRatingClicked = clickObj.itemId;
    }
  }
}

export interface RequestObj {
  userSkills?: SubSkills[];
}
export interface SkillsObject {
  skill?: string;
  userSkills?: SubSkills[];
}
export interface SubSkills {
  rating: string;
  subSkill: string;
  skill: string;
}
/* interface PostResponse {
  responseCode: string;
  message: string;
  uuid: string;
} */
