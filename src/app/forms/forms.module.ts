import { HirerAuthService } from './../core/service/hirer-auth.service';
import { HirerDetailsGuard } from './../core/_guards/hirer-details.guard';
import { MatCardModule } from '@angular/material/card';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { FormsRoutingModule } from "./forms-routing.module";
import { UserSkillsComponent } from "./user-skills/user-skills.component";
import { UserDetailsComponent } from "./user-details/user-details.component";
import { PostProjectComponent } from "./post-project/post-project.component";
import { UserForgotPasswordComponent } from "./user-forgot-password/user-forgot-password.component";
import { UserLoginComponent } from "./user-login/user-login.component";
import { HirerLoginComponent } from "./hirer-login/hirer-login.component";
import { UserDetailsGuard } from "../core/_guards/user-details.guard";
import { RatingComponent } from "../generic/rating/rating.component";
import { SkiconService } from "../core/service/skicon.service";
import { SkiconAuthService } from "../core/service/skicon-auth.service";
import { SkillsRequiredGuard } from "../core/_guards/skills-required.guard";
import { ProjectsService } from "../core/service/projects.service";

import { MatButtonModule } from "@angular/material/button";
import { MatRippleModule } from "@angular/material/core";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatTabsModule } from "@angular/material/tabs";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatStepperModule } from "@angular/material/stepper";
import { MatChipsModule } from "@angular/material/chips";
import { MatIconModule } from "@angular/material/icon";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule } from "@angular/material/core";
import { AmazingTimePickerModule } from "amazing-time-picker";
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { HirerDetailsComponent } from './hirer-details/hirer-details.component';

@NgModule({
  imports: [
    CommonModule,
    FormsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatTabsModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatStepperModule,
    MatChipsModule,
    MatIconModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AmazingTimePickerModule,
    MatPasswordStrengthModule.forRoot(),
    MatCardModule
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatTabsModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatStepperModule,
    MatChipsModule,
    MatIconModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule
  ],
  declarations: [
    UserSkillsComponent,
    UserDetailsComponent,
    PostProjectComponent,
    UserForgotPasswordComponent,
    UserLoginComponent,
    HirerLoginComponent,
    RatingComponent,
    HirerDetailsComponent
  ],
  providers: [
    UserDetailsGuard,
    SkiconService,
    SkiconAuthService,
    SkillsRequiredGuard,
    MatDatepickerModule,
    ProjectsService,
    HirerDetailsGuard,
    HirerAuthService
  ]
})
export class SkiccoFormsModule {}
