import { Component, OnInit } from '@angular/core';
import { trigger, style, transition, animate, keyframes, query, stagger, state } from '@angular/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { SkiconService } from '../../core/service/skicon.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SkiccoLoadingService } from '../../core/service/skicco-loading.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css'],
  providers: [SkiconService],
  animations: [
    trigger('enterAnimation', [
      transition(':enter', [style({ opacity: 0 }), animate(500, style({ opacity: 1 }))]),
      transition(':leave', [animate(700, style({ opacity: 0 }))]),
    ]),
  ],
})
export class UserDetailsComponent implements OnInit {
  currentTab: number = 0;
  prevBtn: boolean = false;
  submitBtn: boolean = false;
  nextBtn = true;
  firstName: string;
  lastName: string;
  mobileNum: string;
  adhar: string;
  doorNum: string;
  zip: string;
  city: string;
  state: string;
  educationalQualification: string;
  specialisation: string;
  avaliableHour: number = 0;
  college: string;
  fromTime: string = ''; /* = "19:00"; */
  toTime: string = ''; /* = "22:00"; */
  distance: string;
  introduction: string;
  submitFormErrors: string[];
  userDetails: UserDetails;
  ALPHABET_REGEX = /^[A-Za-z]+$/;
  NUMBER_REGEX = /^\d+$/;
  updatedAuthToken: string = localStorage.getItem('userDetailsToken') + '204';
  yearOfCompletion: string;
  //errors and alerts

  firstNameError = false;
  firstNameMsg: string;
  lastNameError = false;
  lastNameMsg: string;
  mobileNumError = false;
  mobileNumMsg: string;
  adharError = false;
  adharMsg: string;
  doorNumMsg: string;
  doorNumError = false;
  zipMsg: string;
  zipError = false;
  cityMsg: string;
  cityError = false;
  stateMsg: string;
  stateError = false;
  educationalQualificationMsg: string;
  educationalQualificationError = false;
  specialisationMsg: string;
  specialisationError = false;
  yearOfCompletionMsg: string;
  yearOfCompletionError = false;
  avaliableHourMsg: string;
  avaliableHourError = false;
  collegeMsg: string;
  collegeError = false;
  fromTimeMsg: string;
  fromTimeError = false;
  toTimeMsg: string;
  toTimeError = false;
  distanceMsg: string;
  distanceError = false;
  introductionMsg: string;
  introductionError = false;
  userDetailsFormMsg: string;
  userDetailsFormAlert = false;
  loading = false;
  userDetailsMsg: string;
  userDetailsAlert = false;
  states: string[] = [
    'Andaman and Nicobar Islands',
    'Andhra Pradesh',
    'Arunachal Pradesh',
    'Assam',
    'Bihar',
    'Chandigarh',
    'Chhattisgarh',
    'Dadra and Nagar Haveli',
    'Daman and Diu',
    'Delhi',
    'Goa',
    'Gujarat',
    'Haryana',
    'Himachal Pradesh',
    'Jammu and Kashmir',
    'Jharkhand',
    'Karnataka',
    'Kerala',
    'Lakshadweep',
    'Madhya Pradesh',
    'Maharashtra',
    'Manipur',
    'Meghalaya',
    'Mizoram',
    'Nagaland',
    'Orissa',
    'Pondicherry',
    'Punjab',
    'Rajasthan',
    'Sikkim',
    'Tamil Nadu',
    'Telangana',
    'Tripura',
    'Uttaranchal',
    'Uttar Pradesh',
    'West Bengal',
  ];
  educationQualifications: string[] = [
    '10th class',
    '12th/intermediate',
    'ITI',
    'Diploma',
    'Under graduate(degree)',
    'post graduate(PG)/PGDM',
    'P.hd',
  ];
  hours: any[] = [
    { id: 1, value: '1 hour' },
    { id: 2, value: '2 hours' },
    { id: 3, value: '3 hours' },
    { id: 4, value: '4 hours' },
    { id: 5, value: '5 hours' },
    { id: 6, value: '6 hours' },
    { id: 7, value: '7 hours' },
    { id: 8, value: '8 hours' },
    { id: 9, value: '9 hours' },
  ];
  distances: string[] = [
    'Less than 20 KMs',
    '21 - 50 KMs ',
    ' Greater than 50 Kms',
    'Anywhere in the state',
    'Anywhere in the  country',
  ];
  private notification: any = {
    timeOut: 5000,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true,
    animate: 'fromRight',
  };
  constructor(
    private router: Router,
    private skiconService: SkiconService,
    public snackBar: MatSnackBar,
    private skiccoLoadingService: SkiccoLoadingService,
    private notificationsService: NotificationsService
  ) {}

  ngOnInit() {}

  nextPrev(n) {
    this.currentTab = this.currentTab + n;
    if (this.currentTab == 0) {
      this.prevBtn = false;
    } else if (this.currentTab != 0) {
      this.prevBtn = true;
    }

    if (this.currentTab == 4) {
      this.submitBtn = true;
      this.nextBtn = false;
    } else if (this.currentTab != 4) {
      this.submitBtn = false;
      this.nextBtn = true;
    }
  }

  /* openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  } */
  setCurrentTab(currentTab) {
    this.currentTab = currentTab;
    if (this.currentTab == 0) {
      this.prevBtn = false;
    } else if (this.currentTab != 0) {
      this.prevBtn = true;
    }

    if (this.currentTab == 4) {
      this.submitBtn = true;
      this.nextBtn = false;
    } else if (this.currentTab != 4) {
      this.submitBtn = false;
      this.nextBtn = true;
    }
  }

  userDetailsSubmit() {
    //alert( 'you are bout to submit..!!R u sure.??' );
    this.skiccoLoadingService.show();
    this.submitFormErrors = [];
    this.removeSubitValidations();
    if (this.firstName == null || this.firstName == '') {
      this.firstNameMsg = 'First name should not be empty';
      this.firstNameError = true;
      this.loading = false;
      this.submitFormErrors.push(this.firstNameMsg);
    } else if (this.firstName !== '' && !this.ALPHABET_REGEX.test(this.firstName)) {
      this.firstNameMsg = 'First name should contain only alphabets';
      this.firstNameError = true;
      this.loading = false;
      this.submitFormErrors.push(this.firstNameMsg);
    }
    if (this.lastName == null || this.lastName == '') {
      this.lastNameMsg = 'Last name should not be empty';
      this.lastNameError = true;
      this.loading = false;
      this.submitFormErrors.push(this.lastNameMsg);
    } else if (this.lastName !== '' && !this.ALPHABET_REGEX.test(this.lastName)) {
      this.lastNameMsg = 'Last name should contain only alphabets';
      this.lastNameError = true;
      this.loading = false;
      this.submitFormErrors.push(this.lastNameMsg);
    }
    if (this.mobileNum == null || this.mobileNum == '') {
      this.mobileNumMsg = 'Mobile number should not be empty';
      this.mobileNumError = true;
      this.loading = false;
      this.submitFormErrors.push(this.mobileNum);
    } else if (this.mobileNum !== '' && !this.NUMBER_REGEX.test(this.mobileNum)) {
      this.mobileNumMsg = 'Mobile number is not valid';
      this.mobileNumError = true;
      this.loading = false;
      this.submitFormErrors.push(this.mobileNumMsg);
    }
    if (this.adhar == null || this.adhar == '') {
      this.adharMsg = 'Adhar number should not be empty';
      this.adharError = true;
      this.loading = false;
      this.submitFormErrors.push(this.adharMsg);
    } else if (this.adhar !== '' && !this.NUMBER_REGEX.test(this.adhar)) {
      this.adharMsg = 'Adhar number is not valid';
      this.adharError = true;
      this.loading = false;
      this.submitFormErrors.push(this.adharMsg);
    }
    if (this.doorNum == null || this.doorNum == '') {
      this.doorNumMsg = 'door number should not be empty';
      this.doorNumError = true;
      this.loading = false;
      this.submitFormErrors.push(this.doorNumMsg);
    }
    if (this.city == null || this.city == '') {
      this.cityMsg = 'city should not be empty';
      this.cityError = true;
      this.loading = false;
      this.submitFormErrors.push(this.cityMsg);
    }
    if (this.state == null || this.state == '') {
      this.stateMsg = 'Please select your state/province';
      this.stateError = true;
      this.loading = false;
      this.submitFormErrors.push(this.stateMsg);
    }
    if (this.zip == null || this.zip == '') {
      this.zipMsg = 'zip code should not be empty';
      this.zipError = true;
      this.loading = false;
      this.submitFormErrors.push(this.zipMsg);
    } else if (this.zip !== '' && !this.NUMBER_REGEX.test(this.zip)) {
      this.zipMsg = 'ZIP code is not valid';
      this.zipError = true;
      this.loading = false;
      this.submitFormErrors.push(this.zipMsg);
    }
    if (this.educationalQualification == null || this.educationalQualification == '') {
      this.educationalQualificationMsg = 'educationalQualification should not be empty';
      this.educationalQualificationError = true;
      this.loading = false;
      this.submitFormErrors.push(this.stateMsg);
    }
    if (this.specialisation == null || this.specialisation == '') {
      this.specialisationMsg = 'specialisation should not be empty';
      this.specialisationError = true;
      this.loading = false;
      this.submitFormErrors.push(this.specialisationMsg);
    }
    if (this.yearOfCompletion == null || this.yearOfCompletion == '') {
      this.yearOfCompletionMsg = 'year of completion should not be empty';
      this.yearOfCompletionError = true;
      this.loading = false;
      this.submitFormErrors.push(this.yearOfCompletionMsg);
    }
    if (this.college == null || this.college == '') {
      this.collegeMsg = 'college name should not be empty';
      this.collegeError = true;
      this.loading = false;
      this.submitFormErrors.push(this.collegeMsg);
    }
    if (this.avaliableHour == null || this.avaliableHour == 0) {
      this.avaliableHourMsg = 'avaliable hours should not be empty';
      this.avaliableHourError = true;
      this.loading = false;
      this.submitFormErrors.push(this.avaliableHourMsg);
    }
    if (this.fromTime == null || this.fromTime == '') {
      this.fromTimeMsg = 'From time should not be empty';
      this.fromTimeError = true;
      this.loading = false;
      this.submitFormErrors.push(this.fromTimeMsg);
    }
    if (this.toTime == null || this.toTime == '') {
      this.toTimeMsg = 'To time should not be empty';
      this.toTimeError = true;
      this.loading = false;
      this.submitFormErrors.push(this.toTimeMsg);
    }
    if (this.distance == null || this.distance == '') {
      this.distanceMsg = 'Distance travel should not be empty';
      this.distanceError = true;
      this.loading = false;
      this.submitFormErrors.push(this.distanceMsg);
    }
    if (this.introduction == null || this.introduction == '') {
      this.introductionMsg = 'introduction should not be empty';
      this.introductionError = true;
      this.loading = false;
      this.submitFormErrors.push(this.introductionMsg);
    }

    if (this.submitFormErrors.length == 0) {
      this.userDetails = {
        firstName: this.firstName,
        lastName: this.lastName,
        mobileNumber: this.mobileNum,
        aadharNum: this.adhar,
        doorNumber: this.doorNum,
        state: this.state,
        zipCode: this.zip,
        city: this.city,
        educationQualification: this.educationalQualification,
        educationSpecialization: this.specialisation,
        yearOfCompletion: this.yearOfCompletion,
        noOfHours: this.avaliableHour,
        collegeName: this.college,
        fromTime: this.fromTime,
        toTime: this.toTime,
        distanceTravel: this.distance,
        introduction: this.introduction,
      };

      this.skiconService.saveSkiconDetails(this.userDetails).subscribe(
        (result) => {
          if (result.body == '200') {
            localStorage.setItem('status-token', this.updatedAuthToken);
            this.skiccoLoadingService.hide();
            this.router.navigate(['/forms/user-skills']);
          } else if (result.body == '0') {
            this.skiccoLoadingService.hide();
            this.userDetailsAlert = true;
            this.userDetailsMsg = 'Oops.!! Something went wrong please comeback again..';
            this.notificationsService.error('Server error.', this.userDetailsMsg, this.notification);
          }
        },
        (error) => {
          this.skiccoLoadingService.hide();
          this.userDetailsAlert = true;
          this.userDetailsMsg = 'Oops.!! Something went wrong please comeback again..';
          this.notificationsService.error('Server error.', this.userDetailsMsg, this.notification);
        }
      );
    } else if (this.submitFormErrors.length != 0) {
      this.skiccoLoadingService.hide();
      this.userDetailsAlert = true;
      this.userDetailsMsg = 'There are errors in the form please check.';
      this.snackBar.open(this.userDetailsMsg, 'close', {
        duration: 5000,
      });
      /*  setTimeout(
        function() {
          this.userDetailsAlert = false;
        }.bind(this),
        3000
      ); */
    }
  }
  removeSubitValidations() {
    this.loading = true;
    this.firstNameError = false;
    this.lastNameError = false;
    this.mobileNumError = false;
    this.adharError = false;
    this.doorNumError = false;
    this.zipError = false;
    this.cityError = false;
    this.stateError = false;
    this.educationalQualificationError = false;
    this.specialisationError = false;
    this.yearOfCompletionError = false;
    this.avaliableHourError = false;
    this.collegeError = false;
    this.fromTimeError = false;
    this.toTimeError = false;
    this.distanceError = false;
    this.introductionError = false;
    this.userDetailsFormAlert = false;
    this.userDetailsAlert = false;
  }
}

export interface UserDetails {
  firstName: string;
  lastName: string;
  mobileNumber: string;
  aadharNum: string;
  doorNumber: string;
  city: string;
  zipCode: string;
  state: string;
  educationQualification: string;
  educationSpecialization: string;
  yearOfCompletion: string;
  noOfHours: number;
  collegeName: string;
  fromTime: string;
  toTime: string;
  distanceTravel: string;
  introduction: string;
}
