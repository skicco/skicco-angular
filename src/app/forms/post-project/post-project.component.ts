import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable, Subject } from 'rxjs';
import { Project } from '../../core/model/project';
import { SkillsService } from '../../core/service/skills.service';
import { ProjectsService } from '../../core/service/projects.service';

@Component({
  selector: 'skicco-post-project',
  templateUrl: './post-project.component.html',
  styleUrls: ['./post-project.component.css'],
})
export class PostProjectComponent implements OnInit {
  title: string;
  numberOfResourcesError: string;
  description: string;
  mainSkill: string;
  projectSkills: any[] = [];
  budgetType: string;
  estimatedBudget: string;
  estimatedStartTime: string;
  prefCountry: string;
  cutOffDateForShowInterest: string;
  filesPath: string;
  locationType: string;
  addressLine: string;
  city: string;
  state: string;
  country: string;
  zipCode: string;
  project: Project;
  numberOfResources: string;

  skillDetails: any;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  /* skillCtrl = new FormControl(); */
  filteredsubSkills: Observable<string[]>;
  subSkills: string[] = [];
  subSkillsOfSkill: string[] = [];
  filteredSkills: string[] = [];

  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 0, 1);
  budgets: any[] = [];
  fixedBudget = [
    { name: 'Small patch work (₹250 - ₹500)', value: '250-500' },
    { name: 'Small fix (₹500 - ₹1500)', value: '500-1500' },
    { name: ' Small project (₹10,000 - ₹25,000)', value: '10000-25000' },
    { name: 'Medium project (₹25,000 - ₹1,00,000)', value: '25000-100000' },
    { name: 'Large project (₹1,00,000 - ₹5,00,000)', value: '100000-500000' },
    {
      name: 'Very large project (₹5,00,000 - ₹10,00,000)',
      value: '500000-1000000',
    },
    { name: 'Major project (₹10,00,000 +)', value: '1000000' },
  ];
  hourlyBudget = [
    { name: 'Basic (₹250 - ₹500) per hour per person', value: '250-500' },
    { name: 'Moderate (₹500 - ₹1000) per hour per person', value: '500-1500' },
    {
      name: 'Standard (₹1000- ₹1500) per hour per person',
      value: '10000-25000',
    },
    {
      name: 'Skilled(₹1500 - ₹2000) per hour per person',
      value: '25000-100000',
    },
    { name: 'Expert (₹2500+) per hour per person', value: '100000-500000' },
  ];
  locations = [
    { name: 'Any where from world(remotely)', value: 'remote' },
    { name: 'Prefered country (restrict to a country)', value: 'country' },
    {
      name: 'Specfic place (skicon has to come to the site to work)',
      value: 'onsite',
    },
  ];
  countries = [
    { name: 'India', value: 'india' },
    { name: 'USA', value: 'usa' },
    { name: 'China', value: 'china' },
    { name: 'Russia', value: 'russia' },
  ];

  @ViewChild('subSkillInput') subSkillInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;

  constructor(private skillsService: SkillsService, private projectsService: ProjectsService) {}

  ngOnInit() {
    /* this.filteredsubSkills = this.skillCtrl.valueChanges.pipe(
      startWith(null),
      map((subSkill: string | null) =>
        subSkill ? this._filter(subSkill) : this.subSkillsOfSkill.slice()
      )
    ); */

    this.skillsService.getSkillsDetails().subscribe((result) => {
      this.skillDetails = result;
    });
  }
  setSubSkills(subSkills: any) {
    this.subSkillsOfSkill = [];
    this.subSkills = [];
    this.subSkillsOfSkill = subSkills;
  }
  applyFilter(searchStr: string) {
    this.filteredSkills = this.subSkillsOfSkill.filter((e) => e.toLowerCase().indexOf(searchStr) > -1);
  }
  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;
      //const index = this.subSkillsOfSkill.indexOf(value);
      // Add our fruit
      if ((value || '').trim()) {
        this.subSkills.push(value.trim());
        /* if (index >= 0) {
          this.subSkillsOfSkill.splice(index, 1);
        } */
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      /* this.skillCtrl.setValue(null); */
    }
  }

  remove(subSkill: string): void {
    const index = this.subSkills.indexOf(subSkill);
    if (index >= 0) {
      this.subSkills.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.subSkills.push(event.option.viewValue);
    this.subSkillInput.nativeElement.value = '';
    /* this.skillCtrl.setValue(null); */
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.subSkillsOfSkill.filter((fruit) => fruit.toLowerCase().indexOf(filterValue) === 0);
  }
  setBudget(budgetType: any) {
    switch (budgetType) {
      case 'fixed':
        this.budgets = this.fixedBudget;
        break;
      case 'hourly':
        this.budgets = this.hourlyBudget;
        break;
      case 'mileStone':
        this.budgets = this.fixedBudget;
        break;
    }
  }

  postProject() {
    this.project = {
      huid: '',
      title: this.title,
      skill: this.mainSkill,
      subSkills: this.subSkills,
      numberOfResources: this.numberOfResources,
      budgetType: this.budgetType,
      estimatedBudget: this.estimatedBudget,
      estimatedTime: this.estimatedStartTime,
      description: this.description,
      estimatedStartTime: this.estimatedStartTime,
      locationType: this.locationType,
      prefCountry: this.prefCountry,
      cutoffDateForShowInterest: this.cutOffDateForShowInterest,
      filesPath: this.filesPath,
      addressLine: 'DNo.96-896-78/5', //this.addressLine,
      city: 'Mumbai', //this.city,
      state: 'Maharastra', //this.state,
      country: 'India', //this.country,
      zipCode: '100253', //this.zipCode
    };
    this.projectsService.postProject(this.project).subscribe((response) => {
      if (response.responseCode == '200') {
        alert('success: ' + response.responseCode);
      } else {
        alert('failed');
      }
    });
  }
}
