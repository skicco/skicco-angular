import { Headers } from '@angular/http';
import { DataSharedService } from './../../core/service/data.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SkiccoLoadingService } from '../../core/service/skicco-loading.service';
import { SkiconService } from '../../core/service/index';
import { User } from '../../core/model/user';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css'],
  providers: [SkiconService],
})
export class UserLoginComponent implements OnInit {
  isShowPassword: boolean = false;
  action: string;
  tabIndex: number;
  signupEmail: string;
  isSignUpEmailError: boolean;
  signUpEmailErrorMsg: string;
  signupPassword: string;
  isSignupPasswordErr: boolean;
  signupPasswordErrorMsg: string;
  confirmPassword: string;
  isConfirmPasswordError: boolean;
  confirmPasswordMsg: string;
  signUpErrors: string[];
  user: User;
  alert: string;

  loginErrors: any[];
  loginEmail: string;
  loginPassword: string;
  isLoginEmailErr: boolean;
  loginEmailErrorMsg: string;
  loginPasswordErrorMsg: string;
  isLoginPasswordErr: boolean;
  loginAlert: string;
  showPwdStrength: boolean;

  notification: any = {
    timeOut: 5000,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true,
    animate: 'fromRight',
  };
  //REGEX for Email format verification.
  EMAIL_REGEXP = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i;

  requestTime: any;
  responseTime: any;
  /* emailErrorMsg: string;
  passwordErrorMsg: string;
  cnfPasswordError = false;
  loginMail: string;
  loginEmailErrorMsg: string;
  loginEmailError = false;
  loginPasswordErrorMsg: string;
  loginPasswordError = false;
  loginLoading = false;
  loginError = false;
  loginAlert: string;
  loginErrors: string[];
  hirerSignupEmail: string;
  hirerSignupPassword: string;
  hirerSignupCnfPassword: string;
  hirerSignupEmailError = false;
  hirerSignupEmailMsg: string;
  hirerSignupPasswordMsg: string;
  hirerSignupPasswordError = false;
  hirerSignupAlert = false;
  hirerSignupErrorMsg: string;
  hirerSignupLoading = false;
  hirerSignupErrors: string[];
  hirerCnfPasswordError = false;
  hirerCnfPasswordMsg: string;
  userSignUpBtnText = "Sign Up";
  cnfpasswordtxt = false;
  animationState = "out";
  passwordRequirementsArray: any[] = []; */
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private skiccoLoadingService: SkiccoLoadingService,
    private skiconService: SkiconService,
    private _notificationsService: NotificationsService,
    private dataService: DataSharedService
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.action = params['action'];
      if (this.action == 'signup') {
        this.tabIndex = 0;
      } else if (this.action == 'login') {
        this.tabIndex = 1;
      }
    });
  }

  userSignUp() {
    this.skiccoLoadingService.show();
    this.signUpErrors = [];
    this.removeSignUpValidations();

    if (this.signupEmail == '' || this.signupEmail == null) {
      this.signUpEmailErrorMsg = 'Email should not be empty';
      this.isSignUpEmailError = true;
      this.signUpErrors.push(this.signUpEmailErrorMsg);
    } else if (this.signupEmail !== '' && !this.EMAIL_REGEXP.test(this.signupEmail)) {
      this.signUpEmailErrorMsg = 'Please enter a valid email';
      this.isSignUpEmailError = true;
      this.skiccoLoadingService.hide();
      this.signUpErrors.push(this.signUpEmailErrorMsg);
    }
    if (this.signupPassword == '' || this.signupPassword == null) {
      this.signupPasswordErrorMsg = 'password should not be empty';
      this.isSignupPasswordErr = true;
      this.skiccoLoadingService.hide();
      this.signUpErrors.push(this.signupPasswordErrorMsg);
    }
    if (this.confirmPassword == '' || this.confirmPassword == null) {
      this.confirmPasswordMsg = 'Please confirm the password';
      this.isConfirmPasswordError = true;
      this.skiccoLoadingService.hide();
      this.signUpErrors.push(this.confirmPasswordMsg);
    }

    if (this.confirmPassword !== this.signupPassword) {
      this.confirmPasswordMsg = 'password and confirm password should be same';
      this.isConfirmPasswordError = true;
      this.skiccoLoadingService.hide();
      this.signUpErrors.push(this.confirmPasswordMsg);
    }

    if (this.signUpErrors.length == 0) {
      this.user = {
        emailId: this.signupEmail,
        pwd: this.signupPassword,
      };
      this.skiconService.skiconSignup(this.user).subscribe(
        (result) => {
          console.info(result);
          localStorage.setItem('status-token', result.headers.get('authorization'));

          if (result.body == '201') {
            this.skiccoLoadingService.hide();
            this.router.navigate(['/message'], {
              queryParams: { route: 'userSignUp' },
            });
          } else if (result.body == '202') {
            this.skiccoLoadingService.hide();
            this.alert = 'Please click on the emailLink to Confirm';
            this._notificationsService.error('User already exists.', this.alert, this.notification);
          } else if (result.body == '203') {
            this.skiccoLoadingService.hide();
            this.alert = 'Please login to provide the skillset to continue';
            this._notificationsService.error('User already exists.', this.alert, this.notification);
          } else if (result.body == '204') {
            this.skiccoLoadingService.hide();
            this.alert = 'Please login to enroll into the projects.';
            this._notificationsService.error('User already exists.', this.alert, this.notification);
          } else if (result.body == '0') {
            this.skiccoLoadingService.hide();
            this.alert = 'internal server error';
            this._notificationsService.error('Server error', this.alert, this.notification);
          }
        },
        (error) => {
          ('Oops.!! Something went wrong please comeback again..');
          this.skiccoLoadingService.hide();
          this.alert = 'Oops.!! Something went wrong please comeback again..';
          this._notificationsService.error('Server error', this.alert, this.notification);
        }
      );
    }
  }
  /*******************************************************
   * function to reset all validations on Sign-up submit *
   *******************************************************/
  removeSignUpValidations() {
    this.isConfirmPasswordError = false;
    this.isSignupPasswordErr = false;
    this.isSignUpEmailError = false;
  }

  userLogin() {
    this.requestTime = new Date();
    this.skiccoLoadingService.show();
    this.loginErrors = [];
    this.removeLoginErrors();
    // this.loginAlert = "Email or password is incorrect please check"

    if (this.loginEmail == '' || this.loginEmail == null) {
      this.loginEmailErrorMsg = 'Email should not be empty';
      this.isLoginEmailErr = true;
      this.skiccoLoadingService.hide();
      this.loginErrors.push(this.loginEmailErrorMsg);
    } else if (this.loginEmail !== '' && !this.EMAIL_REGEXP.test(this.loginEmail)) {
      this.loginEmailErrorMsg = 'Email is invalid';
      this.isLoginEmailErr = true;
      this.skiccoLoadingService.hide();
      this.loginErrors.push(this.loginEmailErrorMsg);
    }
    if (this.loginPassword == '' || this.loginPassword == null) {
      this.loginPasswordErrorMsg = 'password should not be empty';
      this.isLoginPasswordErr = true;
      this.skiccoLoadingService.hide();
      this.loginErrors.push(this.loginPasswordErrorMsg);
    }
    this.user = {
      emailId: this.loginEmail,
      pwd: this.loginPassword,
    };
    if (this.loginErrors.length == 0) {
      this.skiconService.skiconLogin(this.user).subscribe(
        (result) => {
          this.responseTime = new Date();
          let time = this.responseTime - this.requestTime;
          let authorization = result.headers.get('Authorization');
          localStorage.setItem('userDetailsToken', authorization);
          if (result.body == '202') {
            localStorage.setItem('status-token', authorization + result.body);
            this.skiccoLoadingService.hide();
            this.loginAlert = 'Please verify your email address by clicking the link in your inbox';
            this._notificationsService.error('Error.', this.loginAlert, this.notification);
          } else if (result.body == '203') {
            localStorage.setItem('status-token', authorization + result.body);
            this.skiccoLoadingService.hide();
            this.router.navigate(['/forms/user-details']);
          } else if (result.body == '204') {
            localStorage.setItem('status-token', authorization + result.body);
            this.skiccoLoadingService.hide();
            this.router.navigate(['/forms/user-skills']);
          } else if (result.body == '205') {
            localStorage.setItem('status-token', authorization + result.body);
            //localStorage.setItem('_userDetails', JSON.stringify(this.postResponse.responseMsg));

            // setUserDetails(,)
            // this.skiccoLoadingService.hide();
            this.router.navigate(['/skicon/dashboard']);
          } else if (result.body == '0') {
            this.skiccoLoadingService.hide();
            this.loginAlert = 'Email / password is incorrect please check';
            this._notificationsService.error('Error.', this.loginAlert, this.notification);
          }
        },
        (error) => {
          this.skiccoLoadingService.hide();
          this.loginAlert = 'Oops.!! Something went wrong please comeback again..';
          this._notificationsService.error('Error.', this.loginAlert, this.notification);
        }
      );
    }
  }

  /***************************************************************
   * Consuming rest API for userLogin after all the validations  *
   ***************************************************************/
  skiconLogin() {}

  /*****************************************************
   * function to reset all validations on Login submit *
   *****************************************************/

  removeLoginErrors() {
    /* this.loginLoading = true;
    this.loginError = false; */
    this.isLoginEmailErr = false;
    this.isLoginPasswordErr = false;
  }

  onStrengthChanged(strength: number) {
    //console.log('password strength = ', strength);
  }
}
