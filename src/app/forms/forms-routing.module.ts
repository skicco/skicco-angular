import { HirerDetailsGuard } from './../core/_guards/hirer-details.guard';
import { HirerDetailsComponent } from './hirer-details/hirer-details.component';
/*
 *
 *  This routing module deals with all the routing related to forms module
 *  skicco.com/forms/...
 */
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { UserSkillsComponent } from "./user-skills/user-skills.component";
import { UserDetailsComponent } from "./user-details/user-details.component";
import { PostProjectComponent } from "./post-project/post-project.component";
import { UserForgotPasswordComponent } from "./user-forgot-password/user-forgot-password.component";
import { UserLoginComponent } from "./user-login/user-login.component";
import { HirerLoginComponent } from "./hirer-login/hirer-login.component";
import { UserDetailsGuard } from "../core/_guards/user-details.guard";
import { SkillsRequiredGuard } from "../core/_guards/skills-required.guard";

const routes: Routes = [
  {
    path: "user-details",
    component: UserDetailsComponent,
    canActivate: [UserDetailsGuard]
  },
  {
    path: "user-skills",
    component: UserSkillsComponent,
    canActivate: [SkillsRequiredGuard]
  },
  {
    path: "hirer-details",
    component: HirerDetailsComponent,
    canActivate: [HirerDetailsGuard]
  },
  { path: "post-project", component: PostProjectComponent },
  { path: "user-forgot-password", component: UserForgotPasswordComponent },
  { path: "user", component: UserLoginComponent },
  { path: "hirer", component: HirerLoginComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule {}
