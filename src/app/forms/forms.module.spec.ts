import { SkiccoFormsModule } from './forms.module';

describe('FormsModule', () => {
  let formsModule: SkiccoFormsModule;

  beforeEach(() => {
    formsModule = new SkiccoFormsModule();
  });

  it('should create an instance', () => {
    expect(formsModule).toBeTruthy();
  });
});
