import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HirerLoginComponent } from './hirer-login.component';

describe('HirerLoginComponent', () => {
  let component: HirerLoginComponent;
  let fixture: ComponentFixture<HirerLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HirerLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HirerLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
