import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {PageNotFoundComponent} from "./errors/page-not-found/page-not-found.component";

const appRoutes: Routes = [
  {
    path: "",
    loadChildren: () => import('./feature/feature.module').then(m => m.FeatureModule)
  },
  {
    path: "forms",
    loadChildren: () => import('./forms/forms.module').then(m => m.SkiccoFormsModule)
  },
  {
    path: "error",
    loadChildren: () => import('./errors/errors.module').then(m => m.ErrorsModule)
  },
  {
    path: "admin",
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: "support",
    loadChildren: () => import('./skicco-support/skicco-support.module').then(m => m.SkiccoSupportModule)
  },
  {
    path: "page-not-found", component: PageNotFoundComponent
  },
  { path: "**", redirectTo: "page-not-found" },

];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
