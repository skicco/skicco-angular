export class Ng2NotificationConstants {
  public static NotificationConstant = {
    timeOut: 9000,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true,
    animate: "fromLeft"
  }
}
