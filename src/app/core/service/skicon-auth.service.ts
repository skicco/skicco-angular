import { HttpClient } from '@angular/common/http';
import { throwError as observableThrowError, Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { AppConfig } from '../config/app.config';
/*
 * @author SriHarsha
 * This class handles all the services related to skicon authentication (user).
 */
@Injectable()
export class SkiconAuthService {
  statusCode: string;
  statusAuthCode: string;

  constructor(private http: HttpClient) {}

  getStatusCode() {
    this.statusCode = localStorage.getItem('status-token');
    if (this.statusCode !== null && this.statusCode !== '' && this.statusCode !== undefined) {
      this.statusAuthCode = this.statusCode.substr(this.statusCode.length - 3);
    }
    return this.statusAuthCode;
  }

  isAuthorized() {
    if (this.getStatusCode() === '203') {
      return true;
    } else {
      return false;
    }
  }

  isSkillsRequired() {
    if (this.getStatusCode() === '204') {
      return true;
    } else {
      return false;
    }
  }
  isNotAuthorized() {
    if (this.getStatusCode() === '202') {
      return true;
    } else {
      return false;
    }
  }

  isSkicon() {
    if (this.getStatusCode() === '205') {
      return true;
    } else {
      return false;
    }
  }
  /* Consuming getUserStatus service */
  userStatus(uuid: string): Observable<any> {
    let userStatus = 'getUserStatus/' + uuid;
    return this.http.get(AppConfig.URL + userStatus).pipe(catchError(this.handleErrorObservable));
  }

  private handleErrorObservable(error: Response | any) {
    return observableThrowError(error.message || error);
  }
}
