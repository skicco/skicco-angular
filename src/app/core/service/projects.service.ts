import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Project } from '../model/project';
import { AppConfig } from '../config/app.config';

/*
 * @author SriHarsha
 * This class handles all the services related to projects.
 */
@Injectable()
export class ProjectsService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  constructor(private http: HttpClient) {}

  postProject(project: Project): Observable<any> {
    let body = JSON.stringify(project);
    let postProject = 'postProject/';
    return this.http
      .post(AppConfig.URL + postProject, body, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem('hirerDetailsToken'),
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }

  retrieveUserSkilledProjects(index: number): Observable<any> {
    let endPointUrl = 'skicon/retrieveskilledprojects/' + index;
    return this.http
      .get(AppConfig.URL + endPointUrl, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem('userDetailsToken'),
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }

  retrieveOtherSkilledProjects(index: number): Observable<any> {
    let endPointUrl = 'skicon/retrieveotherskilledprojects/' + index;
    return this.http
      .get(AppConfig.URL + endPointUrl, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem('userDetailsToken'),
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }
  retrieveHirerProjects(): Observable<any> {
    let endPointUrl = 'hirer/retrieveProjects/';
    return this.http
      .get(AppConfig.URL + endPointUrl, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem('hirerDetailsToken'),
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }

  getFavouriteProjectsList(uuid: any): Observable<any> {
    let endPointUrl = 'skicon/favouriteProjectsList/' + uuid;
    return this.http.get(AppConfig.URL + endPointUrl).pipe(catchError(this.handleErrorObservable));
  }

  private handleErrorObservable(error: Response | any) {
    return observableThrowError(error.message || error);
  }
}
