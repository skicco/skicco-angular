import { HttpClient, HttpHeaders } from '@angular/common/http';

import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { AppConfig } from '../config/app.config';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';

/*
 * @author SriHarsha
 * This class handles all the services related to skills.
 */
@Injectable()
export class SkillsService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  private handleError: HandleError;

  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('skills Service');
  }

  /******************************************
   * Consuming retrieveSkillDetails service *
   ******************************************/
  getSkillsDetails(): Observable<any> {
    let retrieveSkillDetails = 'retrieveSkillDetails/';
    return this.http.get(AppConfig.URL + retrieveSkillDetails).pipe(catchError(this.handleErrorObservable));
  }

  /**
   *
   * @param requestObj
   */
  saveUSerSkillDetails(requestObj: any): Observable<any> {
    let body = JSON.stringify(requestObj);
    let saveUserSkillDetails = 'saveUserSkillDetails/';
    return this.http
      .post(AppConfig.URL + saveUserSkillDetails, body, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem('userDetailsToken'),
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }

  private handleErrorObservable(error: Response | any) {
    return observableThrowError(error.message || error);
  }
}
