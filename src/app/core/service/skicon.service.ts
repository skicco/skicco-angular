import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { catchError } from 'rxjs/operators';
import { User } from '../model/user';
import { AppConfig } from '../config/app.config';

//import { HttpErrorHandler, HandleError } from "./http-error-handler.service";

/*
 * @author SriHarsha
 * This class handles all the services related to skicon (user).
 */
@Injectable()
export class SkiconService {
  constructor(private http: HttpClient) {
    /*  this.handleError = httpErrorHandler.createHandleError("skicon Service"); */
  }

  /********************************
   * Consuming userSignup service *
   ********************************/
  skiconSignup(user: User): Observable<any> {
    let body = JSON.stringify(user);
    let userSignup = 'userSignup/';

    /*   return this.http
      .post(AppConfig.URL + userSignup, body, this.options)
      .pipe(catchError(this.handleError("user Signup", body))); */
    return this.http
      .post(AppConfig.URL + userSignup, body, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }

  //get(this._url).pipe(catchError(this.errorHandler))

  /*******************************
   * Consuming userLogin service *
   *******************************/
  skiconLogin(user: User): Observable<any> {
    let body = JSON.stringify(user);
    let userLogin = 'userlogin/';
    return this.http
      .post(AppConfig.URL + userLogin, body, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }

  /************************************
   * Consuming saveUserDetails service *
   *************************************/
  saveSkiconDetails(userDetails): Observable<any> {
    let body = JSON.stringify(userDetails);
    let saveUserDetails = 'saveUserDetails/';
    return this.http
      .post(AppConfig.URL + saveUserDetails, body, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem('userDetailsToken'),
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }

  /**************************************
   * Consuming userVerification service *
   **************************************/
  userEmailVerification(uuid: string): Observable<any> {
    let userVerification = 'userVerification/' + uuid;
    let timeStamp = +new Date();
    return this.http
      .get(AppConfig.URL + userVerification + '?time=' + timeStamp, { observe: 'response' })
      .pipe(catchError(this.handleErrorObservable));
  }

  /************************************
   * Consuming saveUserDetails service *
   *************************************/
  getSkiconDetails(): Observable<any> {
    let saveUserDetails = 'fetchUserDetails/';
    return this.http
      .get(AppConfig.URL + saveUserDetails, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem('userDetailsToken'),
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }

  getSkiconProfile(uuid: string): Observable<any> {
    return this.http
      .get(AppConfig.URL + 'getSkiconProfile/' + uuid, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem('hirerDetailsToken'),
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleErrorObservable));
  }
  private handleErrorObservable(error: Response | any) {
    return observableThrowError(error.message || error);
  }
}
