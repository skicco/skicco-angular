import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AppConfig } from '../config/app.config';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';
import { HirerDetails } from '../model/hirer-details';

/*
 * @author SriHarsha
 */
@Injectable()
export class HirerService {
  private handleError: HandleError;

  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('hirer Service');
  }

  /**
   *
   * @param hirer
   * consuming hirerSignup rest API
   */
  hirerSignup(hirer): Observable<any> {
    let hirerSignup = 'hirerSignup';

    let body = JSON.stringify(hirer);
    return this.http
      .post(AppConfig.URL + hirerSignup, body, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleError('hirer Signup', body)));
  }

  /**************************************
   * Consuming userVerification service *
   **************************************/
  hirerEmailVerification(huid: string): Observable<any> {
    let hirerVerification = 'hirerVerification/' + huid;
    return this.http
      .get(AppConfig.URL + hirerVerification, { observe: 'response' })
      .pipe(catchError(this.handleError('hirer Verification', [])));
  }

  hirerLogin(hirer: any): Observable<any> {
    let hirerSignup = 'hirerLogin';

    let body = JSON.stringify(hirer);
    console.log('hirer login');
    return this.http
      .post(AppConfig.URL + hirerSignup, body, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleError('hirer Signup', body)));
  }

  saveHirerDetails(hirerDetails: HirerDetails): Observable<any> {
    let body = JSON.stringify(hirerDetails);
    return this.http
      .post(AppConfig.URL + 'saveHirerDetails', body, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: localStorage.getItem('hirerDetailsToken'),
        }),
        observe: 'response',
      })
      .pipe(catchError(this.handleError('hirer Signup', body)));
  }
}
