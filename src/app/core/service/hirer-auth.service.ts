import { throwError as observableThrowError } from 'rxjs';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { AppConfig } from '../config/app.config';
import { HttpClient } from '@angular/common/http';
/*
 * @author SriHarsha
 * This class handles all the services related to skicon authentication (user).
 */
@Injectable()
export class HirerAuthService {
  statusCode: string;
  statusAuthCode: string;

  constructor(private http: HttpClient) {}

  getStatusCode() {
    this.statusCode = localStorage.getItem('hirer-status-token');
    if (this.statusCode) {
      this.statusAuthCode = this.statusCode.substr(this.statusCode.length - 3);
    }
    return this.statusAuthCode;
  }

  isAuthorized() {
    if (this.getStatusCode() === '203') {
      return true;
    } else {
      return false;
    }
  }

  isSkillsRequired() {
    if (this.getStatusCode() === '204') {
      return true;
    } else {
      return false;
    }
  }
  isNotAuthorized() {
    if (this.getStatusCode() === '202') {
      return true;
    } else {
      return false;
    }
  }

  isHirer() {
    if (this.getStatusCode() === '205') {
      return true;
    } else {
      return false;
    }
  }
}
