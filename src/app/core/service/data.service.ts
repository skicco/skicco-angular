import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { ProjectResponse } from "../model/responses/project-response";

@Injectable()
export class DataSharedService {
  private messageSource = new BehaviorSubject("default message");
  currentMessage = this.messageSource.asObservable();
  projectReponse: ProjectResponse;
  retrievedPorjects: any;
  favourityeProjects: any;
  userDashboardDetails: any;
  constructor() {}

  changeMessage(message: string) {
    this.messageSource.next(message);
  }

  setProjectReponseData(data: ProjectResponse) {
    this.projectReponse = data;
  }
  getProjectReponseData() {
    return this.projectReponse;
  }

  setRetrievedProjects(data: any) {}

  getFavourityeProjects() {
    return this.favourityeProjects;
  }
  setFavourityeProjects(favouriteProjects) {
    this.favourityeProjects = favouriteProjects;
  }
}
