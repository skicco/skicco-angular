import { Component, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { SkiccoLoadingService } from "../service/skicco-loading.service";

@Component({
  selector: "skicco-loading",
  templateUrl: "./skicco-loading.component.html",
  styleUrls: ["./skicco-loading.component.css"]
})
export class SkiccoLoadingComponent implements OnInit {
  public loading: boolean = false;
  private subscription: Subscription;
  constructor(private loaderService: SkiccoLoadingService) {}

  ngOnInit() {
    this.subscription = this.loaderService.loaderState.subscribe(
      (state: LoaderState) => {
        this.loading = state.show;
      }
    );
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
export interface LoaderState {
  show: boolean;
}
