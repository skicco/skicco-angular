import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiccoLoadingComponent } from './skicco-loading.component';

describe('SkiccoLoadingComponent', () => {
  let component: SkiccoLoadingComponent;
  let fixture: ComponentFixture<SkiccoLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkiccoLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiccoLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
