import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SkillsService, HirerService, SkiconService } from "./service/index";
import { HttpErrorHandler } from "../core/service/http-error-handler.service";
import { SkiccoLoadingService } from "./service/skicco-loading.service";

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [
    SkillsService,
    HirerService,
    HttpErrorHandler,
    SkiccoLoadingService,
    
  ]
})
export class CoreModule {}
