import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {
  CONTENT_TYPE = 'Content-type';
  CONTENT_TYPE_VALUE = 'application/json';
  AUTHORIZATION = 'Authorization';

  constructor() {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = this.addAuthenticationToken(request);
    return next.handle(request);
  }
  addAuthenticationToken(request: HttpRequest<any>): HttpRequest<any> {
    if (!request.headers.has(this.CONTENT_TYPE)) {
      request = request.clone({
        headers: request.headers.set(this.CONTENT_TYPE, this.CONTENT_TYPE_VALUE),
      });
    }
    request = request.clone({
      headers: request.headers.set(this.AUTHORIZATION, localStorage.getItem('userDetailsToken')),
    });
    return request;
  }
}
