import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";
import { SkiconAuthService } from "../service/skicon-auth.service";
import { Observable } from "rxjs";

@Injectable()
export class UserDetailsGuard implements CanActivate {
  responseCode: any;
  constructor(
    private router: Router,
    private _authService: SkiconAuthService
  ) {}
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this._authService.isAuthorized()) {
      return true;
    } else {
      this.router.navigate(["/"]);
      return false;
    }
  }
}
