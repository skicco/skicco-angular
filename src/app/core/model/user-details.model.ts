export interface UserDetailsModel {
  id: number;
  firstName: string;
  lastName: string;
  mobileNumber: string;
  aadharNumber: string;
  introduction: string;
}
