/*
 * @author SriHarsha
 */
export class Project {
  huid: string;
  title: string;
  skill: string;
  subSkills: string[];
  numberOfResources: string;
  budgetType: string;
  estimatedBudget: string;
  estimatedTime: string;
  description: string;
  estimatedStartTime: string;
  locationType: string;
  prefCountry: string;
  cutoffDateForShowInterest: string;
  filesPath: string;
  addressLine: string;
  city: string;
  state: string;
  country: string;
  zipCode: string;
}
