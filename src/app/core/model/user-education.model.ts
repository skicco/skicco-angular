export interface UserEducation {
  educationQualification: string;
  educationSpecialization: string;
  yearOfCompletion: number;
  collegeName: string;
}
