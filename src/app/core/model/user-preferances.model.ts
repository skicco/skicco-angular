export interface UserPreferances {
  fromTime: string;
  toTime: string;
  distanceTravel: string;
  noOfHours: number;
}
