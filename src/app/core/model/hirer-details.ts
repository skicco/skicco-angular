export class HirerDetails {
  huid: string;
  firstName: string;
  lastName: string;
  clientType: string;
  companyName: string;
  position: string;
  mobileNumber: string;
  isHirerVerified: boolean;
  hirerAddressDTO: {
    addr: string;
    city: string;
    state: string;
    country: string;
    zipCode: string;
  };
}
