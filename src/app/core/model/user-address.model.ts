export interface UserAddress {
  doorNumber: string;
  city: string;
  state: string;
  country: string;
  zipCode: number;
}
