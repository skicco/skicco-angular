import { UserEducation } from "./../user-education.model";
import { UserDetailsModel } from "./../user-details.model";
import { UserPreferances } from "./../user-preferances.model";
import { UserAddress } from "./../user-address.model";
export interface UserDetailsReponse {
  uuid: string;
  name: string;
  userDetails: UserDetailsModel;
  userSkill: string;
  userAddresses: UserAddress[];
  userEducations: UserEducation[];
  userPreferences: UserPreferances;
}
