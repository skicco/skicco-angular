/*
 * @author SriHarsha
 */
export class ProjectResponse {
    "id": number;
    "huid": string;
    "hirerName":string;
    "title": string;
    "skill": string;
    "subSkillsStr": string;
    "subSkills": string[];
    "numberOfResources": number
    "budgetType": string;
    "estimatedBudget":string;
    "estimatedTime": string;
    "description": string;
    "estimatedStartTime": string;
    "prefCountry":string;
    "cutOffDateForShowInterest": string;
    "filesPath": string;
    "locationType": string;
    "addressLine": string;
    "city": string;
    "state": string;
    "country": string;
    "zipCode": string;
    "status": number;
    "genericDetails": genericDetails
}

export interface genericDetails
    {
        "createdBy": string;
        "modifiedBy": string;
        "createdTime": string;
        "modifiedTime": string;
    }
