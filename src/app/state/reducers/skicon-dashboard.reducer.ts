import {
  SkiconDashboardStatus,
  loadSkiconDashboard,
  loadSkiconDashboardSuccess,
} from './../actions/skicon-dashboard.action';
import { ProjectResponse } from './../../core/model/responses/project-response';
import { UserDetailsReponse } from './../../core/model/responses/user-details.response';
import { Action, createReducer, on } from '@ngrx/store';
import * as SkiconDashBoardActions from '../actions/skicon-dashboard.action';

export const featureKey = 'skiconDashboard';
export interface State {
  userDetailsResponse: UserDetailsReponse;
  skilledProjects: ProjectResponse[];
  otherSkiledProjects: ProjectResponse[];
  userStatus: UserStatus;
}

export interface UserStatus {
  isLoading: boolean;
  hasError: boolean;
  notFound: boolean;
}

export const initialState: State = {
  userDetailsResponse: undefined,
  skilledProjects: undefined,
  otherSkiledProjects: undefined,
  userStatus: {
    isLoading: false,
    hasError: false,
    notFound: false,
  },
};

const skiconDashboardboardReducer = createReducer(
  initialState,
  on(SkiconDashBoardActions.loadSkiconDashboard, (state: State) => ({
    ...state,
    userDetailsResponse: undefined,
    skilledProjects: undefined,
    otherSkiledProjects: undefined,
    userStatus: {
      isLoading: true,
      hasError: false,
      notFound: false,
    },
  })),

  on(SkiconDashBoardActions.loadSkiconDashboardSuccess, (state: State, { data }) => ({
    ...state,
    userDetailsResponse: data.userDetailsResponse,
    skilledProjects: data.skilledProjects,
    otherSkiledProjects: data.otherSkiledProjects,
    userStatus: {
      isLoading: false,
      hasError: false,
      notFound: false,
    },
  })),
  on(SkiconDashBoardActions.loadSkiconDashboardFailed, (state: State) => ({
    ...state,
    userDetailsResponse: undefined,
    skilledProjects: undefined,
    otherSkiledProjects: undefined,
    userStatus: {
      isLoading: false,
      hasError: true,
      notFound: false,
    },
  }))
);

export function reducer(state: State | undefined, action: Action) {
  return skiconDashboardboardReducer(state, action); 
}
