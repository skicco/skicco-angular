import { State, featureKey, UserStatus } from './../reducers/skicon-dashboard.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const selectSkiconDashboardState = createFeatureSelector<State>(featureKey);

export const selectSkiconDashboardUserDetails = createSelector(
  selectSkiconDashboardState,
  (state: State) => state.userDetailsResponse
);
export const selectSkiconDashboardSkilledProjects = createSelector(
  selectSkiconDashboardState,
  (state: State) => state.skilledProjects
);
export const selectSkiconDashboardOtherSkilledProjects = createSelector(
  selectSkiconDashboardState,
  (state: State) => state.otherSkiledProjects
);

export const selectSkiconDashboardStatus = createSelector(
  selectSkiconDashboardState,
  (state: State) => state.userStatus
);

export const selectSkiconDashboardIsLoading = createSelector(
  selectSkiconDashboardStatus,
  (status: UserStatus) => status.isLoading
);

export const selectSkiconDashboardHasError = createSelector(
  selectSkiconDashboardStatus,
  (status: UserStatus) => status.hasError
);

export const selectSkiconDashboardNotFound = createSelector(
  selectSkiconDashboardStatus,
  (status: UserStatus) => status.notFound
);
