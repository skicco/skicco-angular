import { SkiconService } from "./../core/service/skicon.service";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { NgModule } from "@angular/core";
import * as fromSkiconDashboard from "./index";

@NgModule({
  declarations: [],
  imports: [
    StoreModule.forRoot({}),
    StoreModule.forFeature(
      fromSkiconDashboard.featureKey,
      fromSkiconDashboard.reducer
    ),
    EffectsModule.forRoot([]),
    EffectsModule.forFeature([fromSkiconDashboard.SkicconDashboardEffects]),
  ],
  providers: [SkiconService, fromSkiconDashboard.SkiconDashboardFacade],
  exports: [],
})
export class StateModule {}
