import { loadSkiconDashboardFailed, loadSkiconDashboard } from './../actions/skicon-dashboard.action';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { User } from './../../core/model/user';
import { SkiconService } from './../../core/service/skicon.service';
import { Injectable } from '@angular/core';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { mergeMap, map } from 'rxjs/operators';
import { EMPTY, Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { Action } from '@ngrx/store';

@Injectable()
export class SkicconDashboardEffects {
  user: User;

  constructor(private actions$: Actions, private skiconService: SkiconService, private router: Router) {}

  loadUserDetails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loadSkiconDashboard),
      mergeMap(() =>
        this.skiconService.skiconLogin(this.user).pipe(
          map((userDetails) => ({ type: '[Skicco API] User details Loaded Success', payload: userDetails })),
          tap(() => this.router.navigate(['/skicon/dashboard'])),
          catchError(() => EMPTY)
        )
      )
    );
  });

  private HandleError(error: HttpErrorResponse): Observable<Action> {
    return of(loadSkiconDashboardFailed());
  }
}
