import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/core/model/user';

export enum SkiconDashboardStatus {
  skiconDashBoard = '[skiconDashboard] skicon dashboard',
  skiconDashboardSuccess = '[skicondashboard] load skicon dashbard success',
  skiconDashBoardError = '[skiconDashboard] load skicon dashboard error',
}

export const loadSkiconDashboardSuccess = createAction(SkiconDashboardStatus.skiconDashboardSuccess, props<{ data: any }>());

export const loadSkiconDashboardFailed = createAction(SkiconDashboardStatus.skiconDashBoardError);
export const loadSkiconDashboard = createAction(SkiconDashboardStatus.skiconDashBoard, props<{ user: User }>());
