import { loadSkiconDashboard } from './../actions/skicon-dashboard.action';
import { UserStatus } from './../reducers/skicon-dashboard.reducer';
import { ProjectResponse } from './../../core/model/responses/project-response';
import {
  selectSkiconDashboardUserDetails,
  selectSkiconDashboardSkilledProjects,
  selectSkiconDashboardOtherSkilledProjects,
  selectSkiconDashboardStatus,
  selectSkiconDashboardIsLoading,
  selectSkiconDashboardHasError,
  selectSkiconDashboardNotFound,
} from './../selectors/skicon-dashboard.selector';
import { select } from '@ngrx/store';
import { UserDetailsReponse } from './../../core/model/responses/user-details.response';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../reducers/skicon-dashboard.reducer';
import { User } from 'src/app/core/model/user';

@Injectable()
export class SkiconDashboardFacade {
  constructor(private readonly store: Store<State>) {}

  userDetails$: Observable<UserDetailsReponse> = this.store.pipe(select(selectSkiconDashboardUserDetails));
  skilledProjects$: Observable<ProjectResponse[]> = this.store.pipe(select(selectSkiconDashboardSkilledProjects));
  otherSkilledProjects$: Observable<ProjectResponse[]> = this.store.pipe(
    select(selectSkiconDashboardOtherSkilledProjects)
  );
  userStatus$: Observable<UserStatus> = this.store.pipe(select(selectSkiconDashboardStatus));
  skiconDashboardIsLoading$: Observable<boolean> = this.store.pipe(select(selectSkiconDashboardIsLoading));
  skiconDashBoardHasError$: Observable<boolean> = this.store.pipe(select(selectSkiconDashboardHasError));
  skiconDashBoardNotFound$: Observable<boolean> = this.store.pipe(select(selectSkiconDashboardNotFound));

  getUserDetails(user: User) {
    this.store.dispatch(loadSkiconDashboard({ user }));
  }
}
