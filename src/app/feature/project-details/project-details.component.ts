import { Component, OnInit } from '@angular/core';
import { DataSharedService } from '../../core/service/data.service';
import { ProjectResponse } from '../../core/model/responses/project-response';
import { Router } from '@angular/router';

@Component({
  selector: 'skicco-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css'],
})
export class ProjectDetailsComponent implements OnInit {
  project: ProjectResponse;
  skills: any;
  isFavourite: boolean;
  constructor(private _dataSharedService: DataSharedService, private router: Router) {}

  ngOnInit() {
    this.project = this._dataSharedService.getProjectReponseData();
    if (this.project === null || this.project === undefined) {
      this.router.navigate(['/skicon/dashboard']);
    }
    this.skills = this.project.subSkills;
  }
}
