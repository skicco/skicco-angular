import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiconProfileComponent } from './skicon-profile.component';

describe('SkiconProfileComponent', () => {
  let component: SkiconProfileComponent;
  let fixture: ComponentFixture<SkiconProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkiconProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiconProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
