import { ActivatedRoute, Params } from '@angular/router';
import { SkiccoLoadingService } from 'src/app/core/service/skicco-loading.service';
import { SkiconService } from 'src/app/core/service';
import { Component, OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'skicco-skicon-profile',
  templateUrl: './skicon-profile.component.html',
  styleUrls: ['./skicon-profile.component.css'],
})
export class SkiconProfileComponent implements OnInit {
  skiconProfile: any;
  userSkills: any[] = [];
  uuid: string;
  constructor(
    private readonly skiconService: SkiconService,
    private readonly skiccoLoadingService: SkiccoLoadingService,
    private readonly activatedRoute: ActivatedRoute
  ) {
    this.skiccoLoadingService.show();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.uuid = params['profile'];
      this.skiconService.getSkiconProfile(this.uuid).subscribe((skiconProfile) => {
        this.skiconProfile = skiconProfile.body;
        this.buildUserSkillList(this.skiconProfile?.userDashBoardDetailsDTO.userSkills);
        this.skiccoLoadingService.hide();
      });
    });
  }

  buildUserSkillList(skills) {
    let temp: any = [];
    let subSkillArray: any[] = [];
    skills.forEach((subSkills) => {
      console.log(subSkills);
      Object.values(subSkills).forEach((subSkillsMap: any[]) => {
        console.log(subSkillsMap);
        subSkillsMap.forEach((subSkill) => {
          this.userSkills.push(subSkill.subSkill);
        });
      });
    });
    this.skiccoLoadingService.hide();
  }

  downloadPdf() {
    console.log('pdf');
    var data = document.getElementById('skicon-profile'); //Id of the table
    html2canvas(data).then((canvas) => {
      // Few necessary setting options
      let imgWidth = 208;
      let imgHeight = (canvas.height * imgWidth) / canvas.width;

      const contentDataURL = canvas.toDataURL('image/png');
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
      let position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save(this.skiconProfile.userDashBoardDetailsDTO.userDetails.firstName + '.pdf'); // Generated PDF
    });
  }
}
