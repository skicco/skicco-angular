import { Component, OnInit } from '@angular/core';
import { SkillsService, SkiconService } from '../../core/service/index';
import { SkiccoLoadingService } from '../../core/service/skicco-loading.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SkiconAuthService } from '../../core/service/skicon-auth.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
  providers: [SkiconAuthService],
})
export class LandingPageComponent implements OnInit {
  /*****************************************
   * Declaring all constants and variables *
   *****************************************/

  cnfPasswordErrorMsg: string;

  skiconHeader: string = 'Skicon';
  skiconPanelmsg1: string = 'Freelance your skill..';
  skiconPanelmsg2: string = 'Register and relax we will get a job for you.';
  hirer: string = 'Hirer';
  hirerPanelMsg1: string = 'Get your work done..!';
  hirerPanelMsg2: string = 'Hire from a large pool of talent and get your work done.';
  email: string;
  password: string;
  cnfPassword: string;
  marqueeMsg: string = '32,904 projects done and 94,904 freelancers registered and counting....';
  //postResponse: PostResponse;
  loading = false;
  returnUrl: 'message';
  snackBar = false;
  alert: string;
  statusToken = localStorage.getItem('status-token');
  constructor(
    /*   private skillsService: SkillsService,
    private skiconService: SkiconService, */
    private skiccoLoadingService: SkiccoLoadingService,
    private router: Router,
    private _authService: SkiconAuthService
  ) {}

  ngOnInit() {
    if (this.statusToken !== null || this.statusToken !== undefined || this.statusToken !== '') {
      var statusCode = this._authService.getStatusCode();
      switch (statusCode) {
        case '203':
          this.router.navigate(['/forms/user-details']);
        case '204':
          this.router.navigate(['/forms/user-skills']);
        case '205':
          this.router.navigate(['/skicon/dashboard']);
          break;
      }
    }
  }
  show() {
    this.skiccoLoadingService.show();
    setTimeout(() => {
      //<<<---    using ()=> syntax
      this.skiccoLoadingService.hide();
    }, 3000);
  }

  userSignup() {
    this.router.navigate(['/forms/user'], {
      queryParams: { action: 'signup' },
    });
  }
  userLogin() {
    this.router.navigate(['/forms/user'], { queryParams: { action: 'login' } });
  }

  hirerLogin() {
    this.router.navigate(['/forms/hirer']);
  }
}
