import { Router, ActivatedRoute, Params } from "@angular/router";
import { OnInit, Component } from "@angular/core";
import { SkiconService } from "../../core/service/index";
import { AlertsService } from "../../core/service/alerts.service";
import { SkiccoLoadingService } from "../../core/service/skicco-loading.service";
import { NotificationsService } from "angular2-notifications";

@Component({
  selector: "app-user-email-verification",
  templateUrl: "./user-email-verification.component.html",
  styleUrls: ["./user-email-verification.component.css"],
  providers: [SkiconService, AlertsService]
})
export class UserEmailVerificationComponent implements OnInit {
  uuid: string;
  loading = true;
  wrong = false;
  messageAlert = false;
  alert: string;
  notification: any = {
    timeOut: 5000,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true,
    animate: "fromRight"
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private skiconService: SkiconService,
    private router: Router,
    private alertsService: AlertsService,
    private _notificationsService: NotificationsService,
    private skiccoLoadingService: SkiccoLoadingService
  ) {}

  ngOnInit() {
    // subscribe to router event
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.skiccoLoadingService.show();
      this.uuid = params["userDetailsToken"];
      if (this.uuid !== null && this.uuid !== "" && this.uuid !== undefined) {
        this.skiconService.userEmailVerification(this.uuid).subscribe(
          result => {
            if (result.body == "200") {
              // this.loading = false;
              this.skiccoLoadingService.hide();
              this.router.navigate([""]);
              this.alertsService.success(
                "Congratulations..!! your email has been verified successfully..please login to continue."
              );
              this.alert =
                " your email has been verified successfully..please login to continue.";
              this._notificationsService.success(
                "Congratulations..!!",
                this.alert,
                this.notification
              );
            } else if (result.body == "203") {
              //this.loading = false;
              this.skiccoLoadingService.hide();
              this.router.navigate([""]);
              this.alertsService.info(
                "you have already verified your email address..please login to continue."
              );
              this.alert =
                " your email has been verified successfully..please login to continue.";
              this._notificationsService.info(
                "Congratulations..!!",
                this.alert,
                this.notification
              );
            }
          },
          error => {
            //this.loading = false;
            this.skiccoLoadingService.hide();
            this.wrong = true;
            this.alertsService.error(
              "Oops.!! Something went wrong please comeback again.."
            );
          }
        );
      } else {
        this.skiccoLoadingService.hide();
        this.router.navigate([""]);
      }
    });
  }
}
