import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";

@Component({
  selector: "app-custom-message",
  templateUrl: "./custom-message.component.html",
  styleUrls: ["./custom-message.component.css"]
})
export class CustomMessageComponent implements OnInit {
  message: string;
  private route: string;
  private userSignUp =
    "Congratulations !! we are very Happy to have you onboard. Please verify your email address through the mail in your mail box";
  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.route = params["route"];
      if (this.route == "userSignUp") {
        this.message = this.userSignUp;
      }
    });
  }
}
