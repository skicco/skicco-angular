import { SkiccoLoadingService } from 'src/app/core/service/skicco-loading.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'skicco-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css'],
})
export class SideNavComponent implements OnInit {
  @Input() userDashBoardDetails: any;
  @Input() userSkills: any;

  constructor(private readonly router: Router, private readonly loadingService: SkiccoLoadingService) {}

  ngOnInit(): void {}
  navigateToUserProfile() {
    this.loadingService.show();
    this.router.navigate(['/skicon-profile'], { queryParams: { profile: this.userDashBoardDetails.uuid } });
  }
}
