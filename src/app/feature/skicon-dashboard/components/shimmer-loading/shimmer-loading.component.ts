import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'skicco-shimmer-loading',
  templateUrl: './shimmer-loading.component.html',
  styleUrls: ['./shimmer-loading.component.css']
})
export class ShimmerLoadingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
