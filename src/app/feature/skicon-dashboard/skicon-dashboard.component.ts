import { SkiconService } from 'src/app/core/service';
import { SkiconDashboardFacade } from './../../state/facade/skicon-dashboard.facade';
import { State } from './../../state/reducers/skicon-dashboard.reducer';
import { UserDetailsReponse } from './../../core/model/responses/user-details.response';
import { DataSharedService } from './../../core/service/data.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectsService } from '../../core/service/projects.service';
import { ProjectResponse } from '../../core/model/responses/project-response';
import { SkiccoLoadingService } from '../../core/service/skicco-loading.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-skicon-dashboard',
  templateUrl: './skicon-dashboard.component.html',
  styleUrls: ['./skicon-dashboard.component.css'],
  providers: [ProjectsService],
})
export class SkiconDashboardComponent implements OnInit {
  tabSelection: number = 0;
  suggestion: number = 0;
  visibleSidebar: boolean = false;
  uuid: string = localStorage.getItem('userDetailsToken');
  matchingProjects: ProjectResponse[] = [];
  matchingProjectsIndex: number = 0;
  otherProjects: ProjectResponse[] = [];
  favouriteProjects: any[] = [];
  displayCategory = 'allProjects';
  userDashBoardDetails: any;
  userSkills: any[] = [];
  otherProjectsSelector: string = '.other-projects';
  otherProjectsIndex: number = 0;
  matchingProjectLoading: boolean;
  otherProjectLoading: boolean;

  /*  */
  sum = 100;
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  /*  */

  userDetail$: Observable<UserDetailsReponse>;
  constructor(
    private router: Router,
    private projectService: ProjectsService,
    private loadingService: SkiccoLoadingService,
    private dataSharedService: DataSharedService,
    private readonly skiconDashboardFacade: SkiconDashboardFacade,
    private skiconService: SkiconService
  ) {
    this.userDetail$ = this.skiconDashboardFacade.userDetails$;
  }

  ngOnInit() {
    this.loadingService.show();
    this.getUserDetails();
    this.getProjects();
  }
  onScrollDown() {
    //this.loadingService.show();
    this.matchingProjectLoading = true;
    this.matchingProjectsIndex++;
    this.projectService.retrieveUserSkilledProjects(this.matchingProjectsIndex).subscribe(
      (projects) => {
        let proj: any[] = projects.body;
        if (proj.length !== 0) {
          projects.body.forEach((pro) => {
            this.matchingProjects.push(pro);
          });
        }
        //this.loadingService.hide();
        this.matchingProjectLoading = false;
      },
      (error) => {
        this.matchingProjectLoading = false;
        //this.loadingService.hide();
        alert('internal server error');
        this.logout();
      }
    );
  }

  onScrollDownOther() {
    //this.loadingService.show();
    this.otherProjectLoading = true;
    this.otherProjectsIndex++;
    this.projectService.retrieveOtherSkilledProjects(this.otherProjectsIndex).subscribe(
      (projects) => {
        let proj: any[] = projects.body;
        if (proj.length !== 0) {
          projects.body.forEach((pro) => {
            this.otherProjects.push(pro);
          });
        }
        // this.loadingService.hide();
        this.otherProjectLoading = false;
      },
      (error) => {
        //this.loadingService.hide();
        alert('internal server error');
        this.logout();
      }
    );
  }
  projectTab(tab: number) {
    this.tabSelection = tab;
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['']);
  }

  showCategory(category: any) {
    if (category == 'favouriteProjects') {
      // this.getFavouriteProjects();
    }

    this.displayCategory = category;
  }

  getFavouriteProjects() {
    this.projectService.getFavouriteProjectsList(this.uuid).subscribe((response) => {
      this.favouriteProjects = response.responseMsg;
    });
  }

  getProjects() {
    this.projectService.retrieveUserSkilledProjects(this.matchingProjectsIndex).subscribe(
      (projects) => {
        this.matchingProjects = projects.body;
        this.loadingService.hide();
      },
      (error) => {
        this.loadingService.hide();
        alert('internal server error');
        this.logout();
      }
    );

    this.projectService.retrieveOtherSkilledProjects(this.otherProjectsIndex).subscribe(
      (projects) => {
        this.otherProjects = projects.body;
        this.loadingService.hide();
      },
      (error) => {
        this.loadingService.hide();
        alert('internal server error');
        this.logout();
      }
    );
  }
  buildUserSkillList() {
    let temp: any = [];
    let subSkillArray: any[] = [];
    this.userDashBoardDetails.userSkills.forEach((subSkills) => {
      Object.values(subSkills).forEach((subSkillsMap: any[]) => {
        subSkillsMap.forEach((subSkill) => {
          this.userSkills.push(subSkill.subSkill);
        });
      });
    });
  }

  getUserDetails() {
    this.skiconService.getSkiconDetails().subscribe((userDetails) => {
      this.userDashBoardDetails = userDetails.body;
      this.buildUserSkillList();
    });
  }
}
