import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiconDashboardComponent } from './skicon-dashboard.component';

describe('SkiconDashboardComponent', () => {
  let component: SkiconDashboardComponent;
  let fixture: ComponentFixture<SkiconDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkiconDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiconDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
