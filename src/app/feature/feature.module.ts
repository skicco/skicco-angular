import { MatListModule } from '@angular/material/list';
import { HirerDashboardModule } from './hirer-dashboard/hirer-dashboard.module';
import { MatTabsModule } from '@angular/material/tabs';
import { StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { FeatureRoutingModule } from './feature-routing.module';
import { SkiconComponent } from './skicon/skicon.component';
import { HirerComponent } from './hirer/hirer.component';
import { AboutComponent } from './about/about.component';
import { SkiconDashboardComponent } from './skicon-dashboard/skicon-dashboard.component';
import { HirerDashboardComponent } from './hirer-dashboard/hirer-dashboard.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { UserEmailVerificationComponent } from './user-email-verification/user-email-verification.component';
import { HirerEmailVerificationComponent } from './hirer-email-verification/hirer-email-verification.component';
import { CustomMessageComponent } from './custom-message/custom-message.component';

import { ProjectDetailsComponent } from './project-details/project-details.component';
import { SkiconProfileComponent } from './skicon-profile/skicon-profile.component';
import { SkiccoProjectComponent } from '../components/skicco-project/skicco-project.component';
import { DashBoardGuard } from '../core/_guards/dashboard.guard';
import { SkiconAuthService } from '../core/service/skicon-auth.service';
import { SkiconCardComponent } from '../components/skicon-card/skicon-card.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import * as fromSkiconBoard from '../state/reducers/skicon-dashboard.reducer';
import { SideNavComponent } from './skicon-dashboard/components/side-nav/side-nav.component';
import { ChildNavComponent } from './skicon-dashboard/components/child-nav/child-nav.component';
import { ShimmerLoadingComponent } from './skicon-dashboard/components/shimmer-loading/shimmer-loading.component';
import { NgxShimmerLoadingModule } from 'ngx-shimmer-loading';
import { ContactComponent } from './contact/contact.component';
@NgModule({
  imports: [
    CommonModule,
    FeatureRoutingModule,
    MatBadgeModule,
    MatCardModule,
    MatPaginatorModule,
    MatTooltipModule,
    MatMenuModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    InfiniteScrollModule,
    NgxShimmerLoadingModule,
    MatListModule,
  ],
  declarations: [
    SkiconComponent,
    HirerComponent,
    AboutComponent,
    SkiconDashboardComponent,
    HirerDashboardComponent,
    LandingPageComponent,
    UserEmailVerificationComponent,
    HirerEmailVerificationComponent,
    CustomMessageComponent,
    ProjectDetailsComponent,
    SkiconProfileComponent,
    SkiccoProjectComponent,
    SkiconCardComponent,
    SideNavComponent,
    ChildNavComponent,
    ShimmerLoadingComponent,
    ContactComponent,
  ],
  exports: [MatBadgeModule, MatCardModule],
  providers: [DashBoardGuard, SkiconAuthService],
})
export class FeatureModule {}
