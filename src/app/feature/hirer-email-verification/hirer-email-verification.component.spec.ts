import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HirerEmailVerificationComponent } from './hirer-email-verification.component';

describe('HirerEmailVerificationComponent', () => {
  let component: HirerEmailVerificationComponent;
  let fixture: ComponentFixture<HirerEmailVerificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HirerEmailVerificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HirerEmailVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
