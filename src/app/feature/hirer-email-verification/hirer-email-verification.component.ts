import { HirerService } from './../../core/service/hirer.service';
import { Params } from '@angular/router';
import { AlertsService } from './../../core/service/alerts.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SkiconService } from 'src/app/core/service';
import { NotificationsService } from 'angular2-notifications';
import { SkiccoLoadingService } from 'src/app/core/service/skicco-loading.service';

@Component({
  selector: 'app-hirer-email-verification',
  templateUrl: './hirer-email-verification.component.html',
  styleUrls: ['./hirer-email-verification.component.css'],
  providers: [HirerService, AlertsService],
})
export class HirerEmailVerificationComponent implements OnInit {
  huid: string;
  loading = true;
  wrong = false;
  messageAlert = false;
  alert: string;
  notification: any = {
    timeOut: 5000,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true,
    animate: 'fromRight',
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private hirerService: HirerService,
    private router: Router,
    private alertsService: AlertsService,
    private _notificationsService: NotificationsService,
    private skiccoLoadingService: SkiccoLoadingService
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.skiccoLoadingService.show();
      this.huid = params['hirerDetailsToken'];
      if (this.huid !== null && this.huid !== '' && this.huid !== undefined) {
        this.hirerService.hirerEmailVerification(this.huid).subscribe(
          (result) => {
            if (result.body == '200') {
              // this.loading = false;
              this.skiccoLoadingService.hide();
              this.router.navigate(['']);
              this.alertsService.success(
                'Congratulations..!! your email has been verified successfully..please login to continue.'
              );
              this.alert = ' your email has been verified successfully..please login to continue.';
              this._notificationsService.success('Congratulations..!!', this.alert, this.notification);
            } else if (result.body == '203') {
              //this.loading = false;
              this.skiccoLoadingService.hide();
              this.router.navigate(['']);
              this.alertsService.info('you have already verified your email address..please login to continue.');
              this.alert = ' your email has been verified successfully..please login to continue.';
              this._notificationsService.info('Congratulations..!!', this.alert, this.notification);
            }
          },
          (error) => {
            //this.loading = false;
            this.skiccoLoadingService.hide();
            this.wrong = true;
            this.alertsService.error('Oops.!! Something went wrong please comeback again..');
          }
        );
      } else {
        this.skiccoLoadingService.hide();
        this.router.navigate(['']);
      }
    });
  }
}
