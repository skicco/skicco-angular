import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkiconComponent } from './skicon.component';

describe('SkiconComponent', () => {
  let component: SkiconComponent;
  let fixture: ComponentFixture<SkiconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkiconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkiconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
