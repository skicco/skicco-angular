import { ContactComponent } from './contact/contact.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SkiconComponent } from './skicon/skicon.component';
import { HirerComponent } from './hirer/hirer.component';
import { AboutComponent } from './about/about.component';
import { SkiconDashboardComponent } from './skicon-dashboard/skicon-dashboard.component';
import { HirerDashboardComponent } from './hirer-dashboard/hirer-dashboard.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { UserEmailVerificationComponent } from './user-email-verification/user-email-verification.component';
import { HirerEmailVerificationComponent } from './hirer-email-verification/hirer-email-verification.component';
import { CustomMessageComponent } from './custom-message/custom-message.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { SkiconProfileComponent } from './skicon-profile/skicon-profile.component';
import { DashBoardGuard } from '../core/_guards/dashboard.guard';

const featureRoutes: Routes = [
  {
    path: '',
    component: LandingPageComponent,
  },
  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'message',
    component: CustomMessageComponent,
  },
  {
    path: 'skicon-profile',
    component: SkiconProfileComponent,
  },
  {
    path: 'project-details',
    component: ProjectDetailsComponent,
  },
  {
    path: 'contact',
    component: ContactComponent,
  },
  {
    path: 'hirer',
    children: [
      {
        path: '',
        component: HirerComponent,
      },
      {
        path: 'dashboard',
        component: HirerDashboardComponent /* ,
        canActivate: [UserDetailsGuard] */,
      },
      {
        path: 'verify-email',
        component: HirerEmailVerificationComponent,
      },
    ],
  },
  {
    path: 'skicon',
    children: [
      {
        path: '',
        component: SkiconComponent,
      },
      {
        path: 'dashboard',
        component: SkiconDashboardComponent,
        canActivate: [DashBoardGuard],
      },
      {
        path: 'verify-email',
        component: UserEmailVerificationComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(featureRoutes)],
  exports: [RouterModule],
})
export class FeatureRoutingModule {}
