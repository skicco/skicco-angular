import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HirerDashboardRoutingModule } from './hirer-dashboard-routing.module';
import { HirerSideNavComponent } from './hirer-side-nav/hirer-side-nav.component';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [HirerSideNavComponent],
  imports: [CommonModule, HirerDashboardRoutingModule, MatListModule],
})
export class HirerDashboardModule {}
