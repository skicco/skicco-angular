import { ProjectsService } from './../../core/service/projects.service';
import { SkiccoLoadingService } from './../../core/service/skicco-loading.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-hirer-dashboard',
  templateUrl: './hirer-dashboard.component.html',
  styleUrls: ['./hirer-dashboard.component.css'],
  providers: [ProjectsService],
})
export class HirerDashboardComponent implements OnInit {
  title = 'progress';
  percentage = '50';
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  visibleSidebar: boolean;
  /* @ViewChild("nav", { read: DragScrollComponent }) ds: DragScrollComponent; */
  items: Array<any> = [];
  displayCategory: string = 'allProjects';
  projects: any[];
  constructor(
    private skiccoLoadingService: SkiccoLoadingService,
    private router: Router,
    private readonly projectsService: ProjectsService
  ) {}

  ngOnInit() {
    this.items = [
      { name: 'assets/images/user-4.jpg' },
      { name: 'assets/images/user-1.jpg' },
      { name: 'assets/images/user-3.jpg' },
      { name: 'assets/images/user-2.jpg' },
      { name: 'assets/images/user-4.jpg' },
      { name: 'assets/images/user-1.jpg' },
      { name: 'assets/images/user-3.jpg' },
      { name: 'assets/images/user-2.jpg' },
      { name: 'assets/images/user-1.jpg' },
      { name: 'assets/images/user-4.jpg' },
      { name: 'assets/images/user-3.jpg' },
      { name: 'assets/images/user-1.jpg' },
    ];

    this.projectsService.retrieveHirerProjects().subscribe((projects) => {
      this.projects = projects.body;
      this.skiccoLoadingService.hide();
    });
  }

  showCategory(n) {}

  switchToSkicon() {
    this.router.navigate(['/']);
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['']);
  }
}
