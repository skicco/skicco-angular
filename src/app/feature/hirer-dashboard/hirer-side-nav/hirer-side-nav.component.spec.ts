import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HirerSideNavComponent } from './hirer-side-nav.component';

describe('HirerSideNavComponent', () => {
  let component: HirerSideNavComponent;
  let fixture: ComponentFixture<HirerSideNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HirerSideNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HirerSideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
