import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HirerDashboardComponent } from './hirer-dashboard.component';

describe('HirerDashboardComponent', () => {
  let component: HirerDashboardComponent;
  let fixture: ComponentFixture<HirerDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HirerDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HirerDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
