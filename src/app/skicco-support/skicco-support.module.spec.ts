import { SkiccoSupportModule } from './skicco-support.module';

describe('SkiccoSupportModule', () => {
  let skiccoSupportModule: SkiccoSupportModule;

  beforeEach(() => {
    skiccoSupportModule = new SkiccoSupportModule();
  });

  it('should create an instance', () => {
    expect(skiccoSupportModule).toBeTruthy();
  });
});
