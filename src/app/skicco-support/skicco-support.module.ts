import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SkiccoSupportRoutingModule } from './skicco-support-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SkiccoSupportRoutingModule
  ],
  declarations: []
})
export class SkiccoSupportModule { }
